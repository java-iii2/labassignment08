package polymorphism;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ElectronicBook e = new ElectronicBook("amongus", "yourmom", 69);
        System.out.println(e);

        BookStore bookstore = new BookStore(5);
        bookstore.bookList[0] = new Book("amongus", "hihi");
        bookstore.bookList[1] = new ElectronicBook("amongus2", "hihi2", 3257);
        bookstore.bookList[2] = new Book("amongus3", "hihi3");
        bookstore.bookList[3] = new ElectronicBook("amongus4", "hihi4", 69);
        bookstore.bookList[4] = new ElectronicBook("amongus5", "hihi5", 420);

        for(int i = 0; i < bookstore.bookList.length; i++){
            System.out.println(bookstore.bookList[i]);
        }

        ElectronicBook b = (ElectronicBook) bookstore.bookList[1];
        System.out.println(b.getNumberBytes());

        ElectronicBook b2 = (ElectronicBook) bookstore.bookList[0];
        System.out.println(b2.getNumberBytes());
    }
}
